#include "userprog/syscall.h"
#include "userprog/process.h"							 
#include <stdio.h>
#include <syscall-nr.h>
#include "devices/shutdown.h"
#include "devices/input.h"						  
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "threads/malloc.h"
#include "filesys/filesys.h"
#include "filesys/file.h"						 

define argue_code 0
define argu_0    4
define argue_1    8
define argue_2    12

#define EXIT_ERROR -1

// making it half buffers 
#define MAX_BUF 512


#define MIN_FD 2

//maping file to there related file
struct file_map
  {                  //list element, file discription & file structure
    struct list_elem elem;      
    int fd;                     
    struct file *file;         
  };


static struct semaphore file_sema;								  
static void syscall_handler (struct intr_frame * c);

static uint32_t load_stack (struct intr_frame *c, int);
bool is_valid_ptr (void *);
bool is_valid_buffer (void *buffer, size_t length);
bool is_valid_string (const char *str);

void sys_halt (void);
void sys_exit (int status);
pid_t sys_exec (const char *cmd_line);
int sys_wait (pid_t pid);
bool sys_create (const char *name, unsigned int initial_size);
bool sys_remove (const char *file);
int sys_open (const char *file);
int sys_filesize (int fd);
int sys_read (int fd, void *buffer, unsigned length);
int sys_write (int fd, const void *buffer, unsigned int length);
void sys_seek (int fd, unsigned position);
unsigned sys_tell (int fd);
void sys_close (int fd);

struct file_map *get_file (int fd);

// it read byte at user virtual address
static int
get_user (const uint8_t *uaddr)
{if ((uint32_t) uaddr >= (uint32_t) PHYS_BASE)
    return -1;
  int marks;
  asm ("movl $1f, %0; movzbl %1, %0; 1:"
       : "=&a" (marks) : "m" (*uaddr));
return marks;}

//bytes are used to addrress 
static bool
put_user (uint8_t *udst, uint8_t byte)
{
  if ((uint32_t) udst < (uint32_t) PHYS_BASE)
    return false;

  int error_code;
  asm ("movl $1f, %0; movb %b2, %1; 1:"
       : "=&a" (error_code), "=m" (*udst) : "q" (byte));
  return error_code != -1;
}
void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

static void
syscall_handler (struct intr_frame *c UNUSED)
{int frame = (int) load_stack (c, argue_code);

  switch (frame)
    {
      case SYS_HALT:
        sys_halt();
        break;

      case SYS_EXIT:
        sys_exit ((int) load_stack (c, argu_0));
        break;

      case SYS_EXEC:
        c->eax = sys_exec ((const char *) load_stack (c, argu_0));
        break;

      case SYS_WAIT:
        c->eax = sys_wait ((pid_t) load_stack (c, argu_0));
        break;

      case SYS_CREATE:
        c->eax = sys_create ((const char *) load_stack (c, argu_0),
          (unsigned int) load_stack (c, argue_1));
        break;

      case SYS_REMOVE:
        c->eax = sys_remove ((const char *) load_stack (c, argu_0));
        break;

      case SYS_OPEN:
        c->eax = sys_open ((const char *) load_stack (c, argu_0));
        break;

      case SYS_FILESIZE:
        c->eax = sys_filesize ((int) load_stack (c, argu_0));
        break;

      case SYS_READ:
        c->eax = sys_read ((int) load_stack (c, argu_0),
            (void *) load_stack (c, ARG_1),
            (unsigned int) load_stack (c, argue_2));
        break;

      case SYS_WRITE:
        c->eax = sys_write ((int) load_stack (c, argu_0),
            (const void *) load_stack (c, argue_1),
            (unsigned int) load_stack (c, argue_2));
        break;

      case SYS_SEEK:
        sys_seek ((int) load_stack (c, argu_0),
          (unsigned) load_stack (c, argue_1));
        break;

      case SYS_TELL:
        c->eax = sys_tell ((int) load_stack (c, argu_0));
        break;

      case SYS_CLOSE:
        sys_close ((int) load_stack (c, argu_0));
        break;

      default:			  
  printf ("system calls...............!\n");
// sys call not found
        sys_exit (EXIT_ERROR);
        break;
    }
} 
//point of the stack is differnet 
static uint32_t
load_stack (struct intr_frame *c, int offset)
{
  if (!is_valid_ptr (c->esp + offset))
    sys_exit (EXIT_ERROR);

  return *((uint32_t *) (c->esp + offset));
}

//checking for user validity
bool
is_valid_ptr (void *vaddr)
{if (get_user ((uint8_t *) vaddr) == -1)
    return false;
  return true;}

//validate for a string
bool
is_valid_buffer (void *buffer, size_t length)
{
  size_t a;
  char *buf = (char *) buffer;

  for (a = 0; a < length; a++)
    {if (!is_valid_ptr (buf + a))
        return false;}
  return true;
}

//validate string from outside
bool
is_valid_string (const char *str)
{int d;
  size_t a = 0;
  while (1)
    {d = get_user ((uint8_t *) (str + a));

      if (d == -1)
        return false;

      if (d == '\0')
        return true;
      a++; }}

//placing pintos
void
sys_halt (void)
{ shutdown_power_off ();}

//process  the user command
void
sys_exit (int status)
{struct thread *cur = thread_current ();
  cur->exit_status = status;
  thread_exit ();}

//run the cmd command
pid_t
sys_exec (const char *cmd_line)
{
  if (!is_valid_string (cmd_line))
    sys_exit (EXIT_ERROR);

  return process_execute (cmd_line);
}

// Waits for the process i
int
sys_wait (pid_t pid)
{return process_wait (pid);}

//creating a new file
bool
sys_create (const char *name, unsigned int initial_size)
{if (!is_valid_string(name))
    sys_exit (EXIT_ERROR);

  bool success;

  sema_down (&file_sema);
  success = filesys_create (name, initial_size);
  sema_up (&file_sema);

  return success;}

//remove file under the name file
bool
sys_remove (const char *file)
{ if (!is_valid_string (file))
    sys_exit (EXIT_ERROR);

  bool success;

  sema_down (&file_sema);
  success = filesys_remove (file);
  sema_up (&file_sema);

  return success;}

//open file user under file
int
sys_open (const char *file)
{
  if (!is_valid_string (file))
    sys_exit (EXIT_ERROR);

  int fd = MIN_FD;
  struct file_map *fm;
  struct thread *cur = thread_current ();

  //checking for unused file
  while (fd >= MIN_FD && get_file (fd) != NULL)
    fd++;


  if (fd < MIN_FD)
    sys_exit (EXIT_ERROR);

  fm = malloc (sizeof (struct file_map));

  if (fm == NULL)
    return -1;
  fm->fd = fd;

  sema_down (&file_sema);
  fm->file = filesys_open (file);
  sema_up (&file_sema);

  if (fm->file == NULL)
    {free (fm);
      return -1;}

  // current thread add new file
  list_push_back (&cur->files, &fm->elem);

  return fm->fd;
}

//check the file size and discription
int
sys_filesize (int fd)
{struct file_map *fm = get_file (fd);
  int size;
  if (fm == NULL)
    return -1;
  sema_down (&file_sema);
  size = file_length (fm->file);
  sema_up (&file_sema);
  return size;}

//reading it to buffer
int
sys_read (int fd, void *buffer, unsigned length)
{size_t a = 0;
  struct file_map *fm;
  int ret;

  // handle reading from stdin separately
  if (fd == STDIN_FILENO)
    { while (a++ < length)
        if (!put_user (((uint8_t *) buffer + a), (uint8_t) input_getc ()))
          sys_exit (EXIT_ERROR);
      return a;}

  if (!is_valid_buffer (buffer, length))
    sys_exit (EXIT_ERROR);
  fm = get_file (fd);
  if (fm == NULL)
    sys_exit (EXIT_ERROR);
  sema_down (&file_sema);
  ret = file_read (fm->file, buffer, length);
  sema_up (&file_sema);
  return ret;}

// read buufer to file
int
sys_write (int fd, const void *buffer, unsigned int length)
{struct file_map *fm;
  unsigned int len;
  char *buf;
  int ret;
  if (!is_valid_buffer ((void *) buffer, length))
    sys_exit (EXIT_ERROR);
  if (fd == STDOUT_FILENO)
    {len = length;
      buf = (char *) buffer;

      while (len > MAX_BUF) {putbuf ((const char *) buf, MAX_BUF);
        len -= MAX_BUF;
        buf += MAX_BUF;}

      putbuf ((const char *) buf, len);
      return length;}

  fm = get_file (fd);
  if (fm == NULL)
    sys_exit (EXIT_ERROR);
  sema_down (&file_sema);
  ret = file_write (fm->file, buffer, length);
  sema_up (&file_sema);

  return ret;}

//move to another location
void
sys_seek (int fd, unsigned position)
{struct file_map *fm = get_file (fd);
  if (fm == NULL)
    return;
sema_down (&file_sema);
  file_seek (fm->file, position);
  sema_up (&file_sema);}

//current possition
unsigned
sys_tell (int fd)
{
  struct file_map *fm = get_file (fd);
  unsigned int ret;

  if (fm == NULL)
    return 0;

  sema_down (&file_sema);
  ret = file_tell (fm->file);
  sema_up (&file_sema);

  return ret;
}

//file close
void
sys_close (int fd)
{
  struct file_map *fm = get_file (fd);

  if (fm == NULL)
    return;

  // close the file
  sema_down (&file_sema);
  file_close (fm->file);
  sema_up (&file_sema);

  // remove it from the thread's list
  list_remove (&fm->elem);

  // deallocate the memory
  free (fm);
}

//file a value
struct file_map *
get_file(int fd)
{
  struct thread *cur = thread_current ();
  struct list_elem *z;
  struct file_map *fm;

  for (z = list_begin (&cur->files); z != list_end (&cur->files);
    z = list_next (z))
    { fm = list_entry (z, struct file_map, elem);
      if (fm->fd == fd)
        return fm; }

  return NULL; }

//file close 
void
close_all_files (struct thread *t)
{struct list_elem *z;
  struct file_map *fm;

  z = list_begin (&t->files);

  while (z != list_end (&t->files))
    {fm = list_entry (z, struct file_map, elem);
      //moving to next element 
      z = list_next (z);
// file close 
sema_down (&file_sema);
      file_close (fm->file);
      sema_up (&file_sema);

      //  remove from list
      list_remove (&fm->elem);
      // reloate
      free (fm); }}